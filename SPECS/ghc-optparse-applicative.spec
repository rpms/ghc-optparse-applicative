# generated by cabal-rpm-0.12.1
# https://fedoraproject.org/wiki/Packaging:Haskell

%global pkg_name optparse-applicative
%global pkgver %{pkg_name}-%{version}

%bcond_without tests

Name:           ghc-%{pkg_name}
Version:        0.14.0.0
Release:        2%{?dist}
Summary:        Utilities and combinators for parsing command line options

License:        BSD
Url:            https://hackage.haskell.org/package/%{pkg_name}
Source0:        https://hackage.haskell.org/package/%{pkgver}/%{pkgver}.tar.gz

BuildRequires:  ghc-Cabal-devel
BuildRequires:  ghc-rpm-macros
# Begin cabal-rpm deps:
BuildRequires:  ghc-ansi-wl-pprint-devel
BuildRequires:  ghc-process-devel
BuildRequires:  ghc-transformers-compat-devel
BuildRequires:  ghc-transformers-devel
%if %{with tests}
BuildRequires:  ghc-QuickCheck-devel
%endif
# End cabal-rpm deps

%description
Utilities and combinators for parsing command line options.

%package devel
Summary:        Haskell %{pkg_name} library development files
Provides:       %{name}-static = %{version}-%{release}
Provides:       %{name}-doc = %{version}-%{release}
%if %{defined ghc_version}
Requires:       ghc-compiler = %{ghc_version}
Requires(post): ghc-compiler = %{ghc_version}
Requires(postun): ghc-compiler = %{ghc_version}
%endif
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
This package provides the Haskell %{pkg_name} library development
files.


%prep
%setup -q -n %{pkgver}


%build
%ghc_lib_build


%install
%ghc_lib_install


%check
%cabal_test


%post devel
%ghc_pkg_recache


%postun devel
%ghc_pkg_recache


%files -f %{name}.files
%license LICENSE


%files devel -f %{name}-devel.files
%doc CHANGELOG.md README.md


%changelog
* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 0.14.0.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Jan 24 2018 Jens Petersen <petersen@redhat.com> - 0.14.0.0-1
- update to 0.14.0.0

* Wed Aug 02 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.13.1.0-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.13.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Thu Feb 23 2017 Jens Petersen <petersen@redhat.com> - 0.13.1.0-1
- update to 0.13.1.0

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 0.12.1.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Sat Jun 25 2016 Jens Petersen <petersen@redhat.com> - 0.12.1.0-1
- update to 0.12.1.0

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 0.11.0.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.11.0.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Wed Nov  5 2014 Jens Petersen <petersen@redhat.com> - 0.11.0.1-3
- disable hlint Annotations on archs without ghci

* Tue Nov  4 2014 Ricky Elrod <relrod@redhat.com> - 0.11.0.1-2
- Add ExclusiveArch for GHCi dependency

* Fri Oct 10 2014 Ricky Elrod <relrod@redhat.com> - 0.11.0.1-1
- Latest upstream release.

* Tue Sep 23 2014 Ricky Elrod <relrod@redhat.com> - 0.10.0-1
- Latest upstream release.

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 0.9.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Wed Jul 30 2014 Ricky Elrod <relrod@redhat.com> - 0.9.0-1
- Latest upstream release.

* Tue Jul  8 2014 Jens Petersen <petersen@redhat.com> - 0.8.1-2
- F21 rebuild

* Mon May 12 2014 Ricky Elrod <rleord@redhat.com> - 0.8.1-1
- Latest upstream release.

* Mon May 5 2014 Jens Petersen <petersen@redhat.com> - 0.8.0.1-2
- add comment about testsuite requiring test-framework

* Thu Apr 10 2014 Fedora Haskell SIG <haskell@lists.fedoraproject.org> - 0.8.0.1-1
- spec file generated by cabal-rpm-0.8.10
